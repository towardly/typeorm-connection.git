import type {
  ObjectLiteral,
  EntityTarget,
  DataSource,
  DataSourceOptions,
  EntityManager,
} from "typeorm";

export type ILoggerOption =
  | boolean
  | "advanced-console"
  | "simple-console"
  | "file"
  | "debug";
type Writable<T> = {
  -readonly [P in keyof T]: T[P];
};

/**
 * 数据库连接对象
 */
export interface ConnectionOptions {
  /** 日志选项, 默认为: true */
  logger?: ILoggerOption;
  /** 连接列表 */
  connections: {
    [index: string]: Partial<Writable<DataSourceOptions>>;
  };
}

/**
 * 初始化数据库连接
 * @param config 连接配置
 */
export declare function connect(config: ConnectionOptions): Promise<void>;
/**
 * 注册 fastify 应用使用 typeorm 连接 mysql
 * @param app FastifyInstance
 * @param config 数据库连接配置
 */
export declare function register_fastify(
  app: any,
  config: ConnectionOptions | Partial<Writable<DataSourceOptions>>
): void;
/**
 * 获取数据源
 * @param entityOrAlias 实体类或者别名
 * @returns
 */
export declare function dataSource<Entity extends ObjectLiteral>(
  entityOrAlias?: EntityTarget<Entity> | string
): DataSource;
/**
 * 获取Entity Manager
 * @param entityOrAlias 实体类或者别名
 * @returns
 */
export declare function manager<Entity extends ObjectLiteral>(
  entityOrAlias?: EntityTarget<Entity> | string
): EntityManager;
/**
 * 断开某个数据库连接
 * @param alias 连接名称
 */
export declare function destroy(alias: string): Promise<void>;
/**
 * 断开所有的连接
 */
export declare function destroyAll(): Promise<void>;

/** 基础数据实体类, 会添加3个固定字段, 自增长主键id、create_time 创建时间、update_time 修改时间 */
export declare abstract class BaseDataEntity {
  /** 自增长主键id */
  id: number;
  /** 创建时间 */
  createTime: Date;
  /** 修改时间 */
  updateTime: Date;
}
