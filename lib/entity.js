var _dec, _dec2, _dec3, _class, _descriptor, _descriptor2, _descriptor3;
function _initializerDefineProperty(target, property, descriptor, context) {
  if (!descriptor) return;
  Object.defineProperty(target, property, {
    enumerable: descriptor.enumerable,
    configurable: descriptor.configurable,
    writable: descriptor.writable,
    value: descriptor.initializer
      ? descriptor.initializer.call(context)
      : void 0,
  });
}
function _applyDecoratedDescriptor(
  target,
  property,
  decorators,
  descriptor,
  context
) {
  var desc = {};
  Object.keys(descriptor).forEach(function (key) {
    desc[key] = descriptor[key];
  });
  desc.enumerable = !!desc.enumerable;
  desc.configurable = !!desc.configurable;
  if ("value" in desc || desc.initializer) {
    desc.writable = true;
  }
  desc = decorators
    .slice()
    .reverse()
    .reduce(function (desc, decorator) {
      return decorator(target, property, desc) || desc;
    }, desc);
  if (context && desc.initializer !== void 0) {
    desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
    desc.initializer = undefined;
  }
  if (desc.initializer === void 0) {
    Object.defineProperty(target, property, desc);
    desc = null;
  }
  return desc;
}
import { Column, PrimaryGeneratedColumn } from "typeorm";

/** 基础数据实体类, 会添加3个固定字段, 自增长主键id、create_time 创建时间、update_time 修改时间 */
export let BaseDataEntity =
  ((_dec = PrimaryGeneratedColumn()),
  (_dec2 = Column({
    type: "datetime",
    default: new Date(),
    update: false,
    name: "create_time",
    comment: "创建时间",
  })),
  (_dec3 = Column({
    type: "datetime",
    default: new Date(),
    name: "update_time",
    comment: "修改时间",
  })),
  ((_class = class BaseDataEntity {
    constructor() {
      /** 自增长主键id */
      _initializerDefineProperty(this, "id", _descriptor, this);
      /** 创建时间 */
      _initializerDefineProperty(this, "createTime", _descriptor2, this);
      /** 修改时间 */
      _initializerDefineProperty(this, "updateTime", _descriptor3, this);
    }
  }),
  ((_descriptor = _applyDecoratedDescriptor(_class.prototype, "id", [_dec], {
    configurable: true,
    enumerable: true,
    writable: true,
    initializer: null,
  })),
  (_descriptor2 = _applyDecoratedDescriptor(
    _class.prototype,
    "createTime",
    [_dec2],
    {
      configurable: true,
      enumerable: true,
      writable: true,
      initializer: null,
    }
  )),
  (_descriptor3 = _applyDecoratedDescriptor(
    _class.prototype,
    "updateTime",
    [_dec3],
    {
      configurable: true,
      enumerable: true,
      writable: true,
      initializer: null,
    }
  ))),
  _class));
