function getOrmLogger(logger) {
  return {
    logQuery(query, parameters) {
      logger.info(`sql: ${query}, parameters: ${parameters}`);
    },
    logQueryError(error, query, parameters) {
      logger.error(error, `query: ${query}, parameters: ${parameters}`);
    },
    logQuerySlow(time, query, parameters) {
      logger.warn(`slow query - query: ${query}, parameters: ${parameters}, time: ${time}`);
    },
    logSchemaBuild(message) {
      logger.info(message);
    },
    logMigration(message) {
      logger.info(message);
    },
    log(level, message) {
      if (level === 'log' || level === 'info') {
        logger.info(message);
      } else if (level === 'warn') {
        logger.warn(message);
      } else if (level === 'error') {
        logger.error(message);
      }
    },
  };
}

/**
 * 获取 typeorm 日志记录器
 * @param logger 日志参数
 * @returns
 */
export function getLogger(logger) {
  if (logger === true) {
    logger = 'advanced-console';
  } else if (typeof logger === 'object') {
    if (logger.log == null && logger.info != null) {
      logger = getOrmLogger(logger);
    }
  }
  return logger;
}
